const service = require('./service')

const initNewsController = server => {
  server.get('/api/news', async (req, res) => {
    try {
      const { data } = await service.search(req.query.q)
      res.status(200).json(data)
    } catch (error) {
      const { status = 500, data } = error.response || {}
      res
        .status(status)
        .json(data || { status: 'error', message: error.message })
    }
  })
}

module.exports = initNewsController
