const express = require('express')
const nock = require('nock')
const request = require('supertest')
const initController = require('../controller')

describe('News API Controller', () => {
  let server

  beforeEach(() => {
    server = express()
    initController(server)
    process.env.API_KEY = 'api-key'
  })

  afterEach(() => {
    delete process.env.API_KEY
    nock.cleanAll()
  })

  describe('Search news', () => {
    test('should search the news api and return it’s response', async () => {
      const scope = nock('https://newsapi.org/v2')
        .get('/top-headlines')
        .query({ country: 'gb', q: 'keywords', apiKey: 'api-key' })
        .reply(200, { status: 'ok' })

      const res = await request(server)
        .get('/api/news?q=keywords')
        .expect('Content-Type', /json/)
        .expect(200)

      expect(scope.isDone()).toBe(true)
      expect(res.body).toEqual({ status: 'ok' })
    })

    test('should pass on news api errors', async () => {
      const scope = nock('https://newsapi.org/v2')
        .get('/top-headlines')
        .query({ country: 'gb', q: '', apiKey: 'api-key' })
        .reply(400, { status: 'error', message: 'error message from api' })

      const res = await request(server)
        .get('/api/news')
        .expect('Content-Type', /json/)
        .expect(400)

      expect(scope.isDone()).toBe(true)
      expect(res.body).toEqual({
        status: 'error',
        message: 'error message from api'
      })
    })

    test('should default error messaging when none returned from the api', async () => {
      const scope = nock('https://newsapi.org/v2')
        .get('/top-headlines')
        .query({ country: 'gb', q: '', apiKey: 'api-key' })
        .reply(400)

      const res = await request(server)
        .get('/api/news?q=')
        .expect('Content-Type', /json/)
        .expect(400)

      expect(scope.isDone()).toBe(true)
      expect(res.body).toEqual({
        status: 'error',
        message: 'Request failed with status code 400'
      })
    })
  })
})
