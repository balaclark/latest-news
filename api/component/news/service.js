const axios = require('axios')
const qs = require('querystring')

const request = axios.create({
  baseURL: 'https://newsapi.org/v2',
  timeout: 5000
})

const get = (path, params) => {
  const query = qs.stringify({
    ...params,
    apiKey: process.env.API_KEY
  })
  return request.get(`${path}?${query}`)
}

const newsService = {
  search: keywords => get('/top-headlines', { q: keywords, country: 'gb' })
}

module.exports = newsService
