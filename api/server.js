const express = require('express')
const server = express()
const cors = require('cors')
const port = process.env.API_PORT || 3000

server.use(cors())

// TODO glob
const initNewsController = require('./component/news/controller')
initNewsController(server)

server.listen(port, () => console.log(`API running: http://localhost:${port}`))
