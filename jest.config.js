module.exports = {
  setupFilesAfterEnv: ['<rootDir>/setup-tests.js'],
  testURL: 'http://localhost',
  testEnvironment: 'jsdom'
}
