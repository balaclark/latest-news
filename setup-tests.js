const Enzyme = require('enzyme')
const Adapter = require('enzyme-adapter-react-16')

Enzyme.configure({ adapter: new Adapter() })
global.XMLHttpRequest = undefined // workaround for axios in the CLI
