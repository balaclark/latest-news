UK News
=======

## Setup

### Node version

This project supports node versions from 8, use nave? Run `nave auto` to get on
the correct version.

### Install dependencies

```
yarn install
```

## Run in development
```
API_KEY=<your newsapi.org api key> yarn dev
```

## Run in production

The API_KEY environment variable should be set on the server.

```
yarn start
```

## Assumptions / Further work

This is a very rough start, the following is missing:

* I would likely use redux for the article state.
* There are no FE tests, I would write those with enzyme.
* I have not styled the app due to lack of time, I would not use bootstrap normally.
* There is no prod build step.
