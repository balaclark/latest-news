import 'babel-polyfill'
import React from 'react'
import { render } from 'react-dom'
import MainLayout from './layout/Main'
import NewsList from './component/News/List'

const App = () => (
  <MainLayout>
    <NewsList />
  </MainLayout>
)

render(<App />, document.getElementById('app'))
