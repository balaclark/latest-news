import React from 'react'
import PropTypes from 'prop-types'

const NewsPreview = ({ url, description, title, urlToImage }) => (
  <a href={url} className="card" style={{ marginBottom: '1em' }}>
    <img src={urlToImage} className="card-img-top" alt="thumbnail" />
    <div className="card-body">
      <h5 className="card-title">{title}</h5>
      <p className="card-text">{description}</p>
    </div>
  </a>
)

NewsPreview.propTypes = {
  url: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  urlToImage: PropTypes.string.isRequired
}

export default NewsPreview
