import React, { useEffect, useState } from 'react'
import axios from 'axios'
import qs from 'querystring'
import debounce from 'lodash.debounce'
import NewsPreview from './Preview'

const NewsList = () => {
  const [isLoading, setIsLoading] = useState(false)
  const [error, setError] = useState()
  const [articles, setArticles] = useState([])

  const search = e => fetchArticles(e.target.value)

  const fetchArticles = debounce(async q => {
    setIsLoading(true)
    setError(null)

    try {
      // TODO move api url to config
      const { data } = await axios.get(
        `http://localhost:3000/api/news?${qs.stringify({
          q
        })}`
      )
      setArticles(data.articles)
    } catch (error) {
      setError(error)
    }

    setIsLoading(false)
  }, 500)

  useEffect(() => {
    fetchArticles()
  }, [])

  return (
    <section>
      <div className="form-group">
        <input
          className="form-control form-control-lg"
          type="text"
          placeholder="Search…"
          onKeyUp={search}
        />
      </div>
      {error && (
        <div className="alert alert-danger" role="alert">
          Oops, something went wrong, please try again.
        </div>
      )}
      {isLoading && <p>Loading…</p>}

      {articles.map((article, i) => (
        <NewsPreview key={`article-${i}`} {...article} />
      ))}
    </section>
  )
}

export default NewsList
