import React from 'react'
import PropTypes from 'prop-types'

const MainLayout = ({ children }) => (
  <section style={{ maxWidth: '980px', margin: 'auto', padding: '2em' }}>
    <header>
      <h1>UK News Search</h1>
    </header>
    {children}
  </section>
)

MainLayout.propTypes = {
  children: PropTypes.node
}

export default MainLayout
